#include <iostream>
#include <vector>
#include <string>
#include "monitor.h"

/*
Przy realizacji zadania do synchronizacji należy wykorzystać semafor
(tylko operacje p i v) – klasa Semaphore zdefiniowana w pliku monitor.h
znajdującym się na stronie przedmiotu. Zadanie należy wykonać z użyciem
wątków (korzystając ze zmiennych globalnych). Bufor 9 elementowy FIFO.
Dwóch konsumentów i dwóch producentów. Producent A produkuje literę A
Producent B produkuje literę B. Konsument A może odczytywać elementy
z bufora jedynie gdy ich liczba jest większa niż 5. Konsument B może
odczytywać elementy z bufora jedynie gdy ich liczba jest większa niż 3.
*/

int const threadsCount = 4;
int const bufferSize = 9;

class Buffer {
private:
	Semaphore mutex;
	Semaphore aWait;
	Semaphore bWait;
	Semaphore emptySpots;

	std::vector<char> values;
	int aAsking = 0;
	int bAsking = 0;
	bool yield = false;

	void print(std::string operation) {
		std::cout << "\n" << operation << ", size: " << values.size() << ", [ ";
		for (char v : values) std::cout << v << " ";
		std::cout << "]\n";
	}

public:
	Buffer() : mutex(1), aWait(0), bWait(0), emptySpots(bufferSize) {}

	void put(char letter) {
		emptySpots.p();
		
		mutex.p();
		values.push_back(letter);
		print(std::string("Put ") + letter);
		if (aAsking > 0 && values.size() > 5) {
			if (bAsking > 0) yield = !yield;
			
			if (bAsking > 0 && yield) {
				--bAsking;
				bWait.v();			
			}
			else {
				--aAsking;
				aWait.v();		
			}
		}
		else if (bAsking > 0 && values.size() > 3) {
			--bAsking;
			bWait.v();
		}
		else {
			mutex.v();
		}
	}

	char getA() {
		mutex.p();
		if (values.size() <= 5) {
			++aAsking;
			mutex.v();
			
			aWait.p();
		}

		values.erase(values.begin());
		print("Consumed by A");
		emptySpots.v();
		mutex.v();
		
		return 'A';
	}

	char getB() {
		mutex.p();
		if (values.size() <= 3) {
			++bAsking;
			mutex.v();
			
			bWait.p();
		}
		
		values.erase(values.begin());
		print("Consumed by B");
		emptySpots.v();
		mutex.v();

		return 'B';
	}
};

Buffer buffer;

void* threadProdA(void* arg) {
	for (int i = 0; i < 20; ++i) buffer.put('A');
	return NULL;
}

void* threadProdB(void* arg) {
	for (int i = 0; i < 20; ++i) buffer.put('B');
	return NULL;
}

void* threadConsA(void* arg) {
	for (int i = 0; i < 100; ++i) char value = buffer.getA();
	return NULL;
}

void* threadConsB(void* arg) {
	for (int i = 0; i < 100; ++i) char value = buffer.getB();
	return NULL;
}

int main() {
	pthread_t tid[threadsCount];

	pthread_create(&(tid[0]), NULL, threadProdA, NULL);
	pthread_create(&(tid[1]), NULL, threadProdB, NULL);
	pthread_create(&(tid[2]), NULL, threadConsA, NULL);
	pthread_create(&(tid[3]), NULL, threadConsB, NULL);

	for (int i = 0; i < threadsCount; i++) pthread_join(tid[i], (void**)NULL);
	return 0;
}
