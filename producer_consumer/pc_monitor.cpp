#include <iostream>
#include <vector>
#include <string>
#include "monitor.h"

/*
Zadanie należy wykonać z użyciem
wątków (korzystając ze zmiennych globalnych). Bufor 9 elementowy FIFO.
Dwóch konsumentów i dwóch producentów. Producent A produkuje literę A
Producent B produkuje literę B. Konsument A może odczytywać elementy
z bufora jedynie gdy ich liczba jest większa niż 5. Konsument B może
odczytywać elementy z bufora jedynie gdy ich liczba jest większa niż 3.
*/

int const threadsCount = 4;
int const bufferSize = 9;

class Buffer : Monitor {
private:
	std::vector<char> values;
	Condition aWait;
	Condition bWait;
	Condition prodWait;

	void print(std::string operation) {
		std::cout << "\n" << operation << ", size: " << values.size() << ", [ ";
		for (char v : values) std::cout << v << " ";
		std::cout << "]\n";
	}

public:
	Buffer() {}
	bool canProd() { return values.size() < bufferSize; }
	bool canA()    { return values.size() > 5; }
	bool canB()    { return values.size() > 3; }

	void put(char letter) {	
		enter();
		
		if (!canProd()) wait(prodWait);
		
		values.push_back(letter);
		print(std::string("Put ") + letter);
		
		if (canA()) signal(aWait);
		if (canB()) signal(bWait);
		
		leave();
	}

	char getA() {
		enter();

		if (!canA()) wait(aWait);

		values.erase(values.begin());
		print("Consumed by A");
		
		signal(prodWait);
		
		leave();	
		return 'A';
	}

	char getB() {
		enter();

		if (!canB()) wait(bWait);

		values.erase(values.begin());
		print("Consumed by B");
		
		signal(prodWait);
		
		leave();	
		return 'B';
	}
};

Buffer buffer;

void* threadProdA(void* arg) {
	for (int i = 0; i < 20; ++i) buffer.put('A');
	return NULL;
}

void* threadProdB(void* arg) {
	for (int i = 0; i < 20; ++i) buffer.put('B');
	return NULL;
}

void* threadConsA(void* arg) {
	for (int i = 0; i < 100; ++i) char value = buffer.getA();
	return NULL;
}

void* threadConsB(void* arg) {
	for (int i = 0; i < 100; ++i) char value = buffer.getB();
	return NULL;
}

int main() {
	pthread_t tid[threadsCount];

	pthread_create(&(tid[0]), NULL, threadProdA, NULL);
	pthread_create(&(tid[1]), NULL, threadProdB, NULL);
	pthread_create(&(tid[2]), NULL, threadConsA, NULL);
	pthread_create(&(tid[3]), NULL, threadConsB, NULL);

	for (int i = 0; i < threadsCount; i++) pthread_join(tid[i], (void**)NULL);
	return 0;
}
