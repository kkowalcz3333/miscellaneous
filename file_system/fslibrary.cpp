#include <stdio.h>
#include <fstream>
#include <cassert>
#include <vector>
#include <iostream>
#include <string>

typedef uint16_t word;

constexpr short DESCRIPTORS_OFFSET = 0;
constexpr short TRANSACTION_OFFSET = 54;
constexpr short ALLOCATION_TABLE_OFFSET = 56;
constexpr short MEMORY_OFFSET = 160;

constexpr short DESCRIPTOR_COUNT = 27;
constexpr short MEMORY_BLOCK_COUNT = 118;
constexpr short WORD_BITS = 16;

constexpr short FREE_BYTES_LENGTH = 4;
constexpr short FREE_BYTES_MASK = 0b00001111;
constexpr short NAME_LENGTH = 5;
constexpr short NAME_MASK = 0b00011111;
constexpr short NAME_MODULO = 32;
constexpr short DESCRIPTOR_INDEX_LENGTH = 5;
constexpr short DESCRIPTOR_INDEX_MASK = 0b00011111;
constexpr short ALLOCATION_FIELD_LENGTH = 7;
constexpr short ALLOCATION_FIELD_MASK = 0b01111111;

constexpr short BLOCK_SIZE = 16;
constexpr short FINAL_BLOCK = 126;
constexpr short UNALLOCATED_BLOCK = 127;

constexpr short TRANSACTION_NONE = 2047;
constexpr short TRANSACTION_DELETE = 2046;

bool contains(const std::vector<short>& data, short value) {
	for (short i : data) {
		if (i == value) {
			return true;
		}
	}
	return false;
}

class Transaction {
friend class FSLibrary;
private:
	short descriptor_index;
	short new_size;

	Transaction(short index, short size) : descriptor_index(index), new_size(size) {}

	Transaction(word data) {
		descriptor_index = data & DESCRIPTOR_INDEX_MASK;
		new_size = data >> DESCRIPTOR_INDEX_LENGTH;
	}

	word pack() {
		return (new_size << DESCRIPTOR_INDEX_LENGTH) + descriptor_index;
	}
};

class FileHandle {
friend class FSLibrary;
private:
	short descriptor_index;
	short offset;

	FileHandle() : descriptor_index(-1), offset(-1) {}
	FileHandle(short arg_descriptor, short arg_offset) : descriptor_index(arg_descriptor), offset(arg_offset) {}
public:

	bool is_valid() {
		return descriptor_index != -1;
	}
};

class FileDescriptor {
friend class FSLibrary;
private:
	char name;
	short free_bytes;
	short size;
	std::vector<short> allocation_fields;

	FileDescriptor() {}

	FileDescriptor(char arg_name) {
		set_size(0);
		name = arg_name & NAME_MASK;
	}

	FileDescriptor(word data) {
		short first_block = data & ALLOCATION_FIELD_MASK;
		if (first_block != FINAL_BLOCK) allocation_fields.push_back(first_block);
		free_bytes = (data >> ALLOCATION_FIELD_LENGTH) & FREE_BYTES_MASK;
		size = 0;
		name = (data >> (ALLOCATION_FIELD_LENGTH + FREE_BYTES_LENGTH)) & NAME_MASK;
	}

	void set_size(short new_size) {
		size = new_size;
		free_bytes = BLOCK_SIZE - (new_size % BLOCK_SIZE);
	}

	bool is_invalid() {
		return allocation_fields[0] == UNALLOCATED_BLOCK;
	}

	bool is_fresh() {
		return allocation_fields.size() == 0;
	}

	bool exists() {
		return is_fresh() || !is_invalid();
	}

	word pack() {
		short field = is_fresh() ? FINAL_BLOCK : allocation_fields[0];
		short bytes = is_fresh() ? 0 : free_bytes;
		return (((name << FREE_BYTES_LENGTH) + bytes) << ALLOCATION_FIELD_LENGTH) + field;
	}
};

class FSLibrary {
private:
	std::fstream fs_handle;

	FileDescriptor descriptors[DESCRIPTOR_COUNT];
	bool memory_blocks_taken[MEMORY_BLOCK_COUNT];

	void add_new_blocks() {
		Transaction transaction(get_word(TRANSACTION_OFFSET));
		short index = transaction.descriptor_index;
		short new_size = transaction.new_size;
		FileDescriptor *descriptor = &descriptors[transaction.descriptor_index];

		if (descriptor->is_fresh()) {
			short new_block = find_empty_block();
			memory_blocks_taken[new_block] = true;
			descriptor->allocation_fields.push_back(new_block);
			set_word(DESCRIPTORS_OFFSET + index * 2, descriptor->pack());
		}
		if (get_allocation_field(descriptor->allocation_fields.front()) == UNALLOCATED_BLOCK) {
			set_allocation_field(descriptor->allocation_fields.front(), FINAL_BLOCK);
		}
		
		short old = descriptor->allocation_fields.back();
		for (int i = 0; i < (new_size - 1) / 16 + 1 - descriptor->allocation_fields.size(); ++i) {
			short field = find_empty_block();
			set_allocation_field(old, field);
			memory_blocks_taken[field] = true;
			descriptor->allocation_fields.push_back(field);
			old = field;
		}
		set_allocation_field(old, FINAL_BLOCK);

		descriptor->set_size(new_size);
		set_word(DESCRIPTORS_OFFSET + index * 2, descriptor->pack());
		set_word(TRANSACTION_OFFSET, Transaction(0, TRANSACTION_NONE).pack());
	}

	void finish_transaction(Transaction&& transaction) {
		if (transaction.new_size == TRANSACTION_NONE) return;
		else if (transaction.new_size == TRANSACTION_DELETE) { /*delete()*/ }
		else add_new_blocks();
	}

	word get_word(short offset) {
		assert(offset % 2 == 0);
		word result;
		fs_handle.seekg(offset);
		fs_handle.read((char*) &result, 2);
		return (result << 8) + (result >> 8);
	}

	void set_word(short offset, word value) {
		assert(offset % 2 == 0);
		fs_handle.seekp(offset);
		value = (value >> 8) + (value << 8);
		fs_handle.write((char*) &value, 2);
	}


	short get_allocation_field(short index) {
		short offset = ALLOCATION_TABLE_OFFSET + (index * ALLOCATION_FIELD_LENGTH / 8);
		short shift_bits = WORD_BITS - ALLOCATION_FIELD_LENGTH - (index * ALLOCATION_FIELD_LENGTH % 8);
		if (offset % 2 == 0) {
			return (get_word(offset) >> shift_bits) & ALLOCATION_FIELD_MASK;
		}
		word unaligned = (get_word(offset - 1) << 8) + (get_word(offset + 1) >> 8);
		return (unaligned >> shift_bits) & ALLOCATION_FIELD_MASK;
	}

	void set_allocation_field(short index, short value) {
		if (index == FINAL_BLOCK) return;

		short offset = ALLOCATION_TABLE_OFFSET + (index * ALLOCATION_FIELD_LENGTH / 8);
		short shift_bits = WORD_BITS - ALLOCATION_FIELD_LENGTH - (index * ALLOCATION_FIELD_LENGTH % 8);
		if (offset % 2 == 0) {
			word result = get_word(offset) & ~(ALLOCATION_FIELD_MASK << shift_bits);
			result += value << shift_bits;
			set_word(offset, result);
		}
		else {
			word w1 = get_word(offset - 1);
			word w2 = get_word(offset + 1);
			word unaligned = (w1 << 8) + (w2 >> 8);
			word a = ~(ALLOCATION_FIELD_MASK << shift_bits);
			word result = unaligned & a;
			result += value << shift_bits;
			w1 = (w1 & 0xff00) + (result >> 8);
			w2 = (w2 & 0x00ff) + (result << 8);
			set_word(offset - 1, w1);
			set_word(offset + 1, w2);
		}
	}

	void resolve_allocation_fields(short descriptor_index) {
		FileDescriptor *descriptor = &descriptors[descriptor_index];
		if (descriptor->is_fresh() || descriptor->is_invalid()) return;

		short field = descriptor->allocation_fields[0];
		descriptor->allocation_fields = {};
		short amount = 0;
		while (field != FINAL_BLOCK && field != UNALLOCATED_BLOCK) {
			descriptor->allocation_fields.push_back(field);
			memory_blocks_taken[field] = true;
			field = get_allocation_field(field);
			++amount;
		}
		descriptor->set_size(amount * BLOCK_SIZE - descriptor->free_bytes);
	}

	short find_empty_block() {
		for (int i = 0; i < MEMORY_BLOCK_COUNT; ++i) {
			if (!memory_blocks_taken[i]) return i;
		}
		return -1;
	}

	short append_empty_block(std::vector<short>& newly_taken) {
		for (int i = 0; i < MEMORY_BLOCK_COUNT; ++i) {
			if (!memory_blocks_taken[i] && !contains(newly_taken, i)) {
				newly_taken.push_back(i);
				return i;
			}
		}
		return -1;
	}

	void resolve_descriptors() {
		for (int i = 0; i < DESCRIPTOR_COUNT; ++i) {
			descriptors[i] = FileDescriptor(get_word(i * 2));
			resolve_allocation_fields(i);
		}
	}

	short find_descriptor(char name) {
		for (int i = 0; i < DESCRIPTOR_COUNT; ++i) {
			if (descriptors[i].exists() && descriptors[i].name == name % NAME_MODULO) return i;
		}
		return -1;
	}

	void unmount() {
		fs_handle.close();
		exit(0);
	}

public:
	FSLibrary(std::string fs_path) {
		fs_handle.open(fs_path);
		assert (fs_handle);

		std::fill(memory_blocks_taken, memory_blocks_taken + MEMORY_BLOCK_COUNT, false);
		resolve_descriptors();

		finish_transaction(Transaction(get_word(TRANSACTION_OFFSET)));
	}

	~FSLibrary() {
		fs_handle.close();
	}
	
	std::vector<std::string> ls() {
		std::vector<std::string> result;
		for (FileDescriptor& descriptor : descriptors) {
			if (descriptor.exists()) {
				result.push_back(std::string(1, descriptor.name + 64) + ": " + std::to_string(descriptor.size) + " B");
			}
		}
		return result;
	}

	short create_file(char name) {
		if (find_descriptor(name) != -1) return -1;

		for (short i = 0; i < DESCRIPTOR_COUNT; ++i) {
			if (!descriptors[i].exists()) {
				descriptors[i] = FileDescriptor(name);
				set_word(DESCRIPTORS_OFFSET + i * 2, descriptors[i].pack());

				return 0;
			}
		}
		return -1;
	}

	FileHandle open_file(char name) {
		short index = find_descriptor(name);
		if (index == -1) return FileHandle();
		return FileHandle(index, 0);
	}

	template<typename T> short append(FileHandle& handle, const T& data) {
		if (!handle.is_valid()) return -1;
		FileDescriptor *descriptor = &descriptors[handle.descriptor_index];
		const char* bytes = reinterpret_cast<const char*>(&data);
		short size = sizeof(data);
		
		short new_block_count = (descriptor->size + size - 1) / BLOCK_SIZE + 1 - descriptor->allocation_fields.size();
		std::vector<short> new_blocks;
		while (new_block_count--) {
			if (append_empty_block(new_blocks) == -1) {
				return -1;
			}
		}
		
		short i = 0;
		short stop_offset = descriptor->is_fresh() ? 0 : MEMORY_OFFSET + BLOCK_SIZE * (1 + descriptor->allocation_fields.back());
		short offset = descriptor->is_fresh() ? 0 : stop_offset - descriptor->free_bytes;

		if (descriptor->size % 2 == 1) {
			// no stop_offset - the byte to write is in the same block
			--offset;
			set_word(offset, (get_word(offset) & 0xff00) + bytes[i++]);
			offset += 2;
		}

		while (offset < stop_offset && i < size) {
			// might write 1 byte too much but it's invalid anyway
			set_word(offset, (bytes[i++] << 8) + bytes[i++]);
			offset += 2;
		}
		
		for (short j : new_blocks) {
			offset = MEMORY_OFFSET + j * BLOCK_SIZE;
			stop_offset = offset + BLOCK_SIZE;
			while (offset < stop_offset && i < size) {
				set_word(offset, (bytes[i++] << 8) + bytes[i++]);
				offset += 2;
			} 
		}
		
		descriptor->set_size(descriptor->size + size);
		handle.offset += size;
		set_word(TRANSACTION_OFFSET, Transaction(handle.descriptor_index, descriptor->size).pack());
		add_new_blocks();
		return 0;
	}
};

int main() {
	FSLibrary library("card");
	library.create_file('X');
	for (std::string descriptor : library.ls()) std::cout << descriptor << std::endl;
	FileHandle handle = library.open_file('X');
	library.append(handle, (int) 7);
	library.append(handle, (int) 7);
	library.append(handle, (int) 7);
	library.append(handle, 'A');
	library.append(handle, (int) 7);
	
	return 0;
}
