# Program wczytuje plik z image.bmp (standardowa 24-bitowa mapa bez kompresji, dowolne realistyczne wymiary, rozmiar naglowka do 2046 B)
# W tym samym pliku jest zapisywany wygladzony obraz (srednia z okna 5x5 po odrzuceniu 5 minimalnych i 5 maksymalnych wartosci)
# Jezeli okno jest na brzegu i nie ma 25 pikseli, przetwarzane sa te, ktore istnieja, a mediana usuwa wartosci do momentu uzyskania 15 pikseli
# (jezeli ilosc pikseli jest nieparzysta, zostaje 1 mala wartosc wiecej); kazdy kolor jest liczony osobno
# Obliczane piksele sa gromadzone w buforze (max 2046 B) i kawalkami zapisywane do pliku

# pobieranie slowa i operowanie na jego zawartosci
	.eqv	LOAD, 1024
	.eqv	SEEK, 62
	.eqv	READ, 63
	.eqv	WRITE, 64
	.eqv	CLOSE, 57
	.eqv	EXIT, 10
	.eqv	SBRK, 9
	.eqv	READ_MODE, 0
	.eqv	WRITE_MODE, 1
	
	.data
file_descriptor:
	.space	4
filter_buffer_red:
	.space 28			# 25, ale 28 dla wyrownania
filter_buffer_green:
	.space 28
filter_buffer_blue:
	.space 28
path:	
	.asciz	"image.bmp"
	.text 
	la	a0, path		# otwarcie image.bmp
	li	a1, READ_MODE
	li	a7, LOAD
	ecall
	mv	t6, a0			# t6: file descriptor
	mv	a0, t6			# wczytywanie rozmiaru pliku i naglowka
	la	t5, filter_buffer_red	# tymczasowo bufor na pierwsze dane, t5: adres poczatku
	addi	a1, t5, 2
	li 	a2, 14
	li	a7, READ
	ecall							
	lw	t3, 4(t5)		# t3: rozmiar tablicy pikseli, t4: naglowka
	lw	t4, 12(t5)
	sub	t3, t3, t4
	mv	a0, t6			# przesuniecie kursora z powrotem na poczatek pliku
	li	a1, 0
	li	a2, 0
	li	a7, SEEK
	ecall
	mv	a0, t3			# alokowanie miejsca na tablice pikseli
	li	a7, SBRK
	ecall
	mv	s7, a0
	addi	a0, t3, 6		# a0: rozmiar alokowanego bufora: max(t3+6, t4+2), limit 2046 B
	addi	t4, t4, 2		# +6, by male pliki nie zapisywaly sie na 2 razy, +2, by mozna bylo poprawnie zapisac naglowek
	bge	a0, t4, limit
	mv	a0, t4
limit:	li	t1, 2046
	ble	a0, t1, allocate
	li	a0, 2046
allocate:				# alokowanie miejsca na naglowek/bufor nowych pikseli
	addi	s1, a0, -6
	li	a7, SBRK
	ecall
	mv	s8, a0
	mv	a0, t6			# zapis naglowka do drugiego bufora
	addi	a1, s8, 2		# wyrownanie do 4B
	addi	a2, t4, -2
	li	a7, READ
	ecall	
	mv	a0, t6			# zapis tablicy pikseli do pierwszego bufora
	mv	a1, s7
	mv	a2, t3
	li	a7, READ
	ecall				
	mv	a0, t6			# zamkniecie pliku
	li	a7, CLOSE
	ecall
	la	a0, path		# otwarcie pliku do zapisu
	li	a1, WRITE_MODE
	li	a7, LOAD
	ecall
	sw	a0, file_descriptor, t0
	addi	a1, s8, 2		# przepisanie naglowka
	addi	a2, t4, -2
	li	a7, WRITE		
	ecall
	lw	s10, 20(s8)		# tymczasowo szerokosc i wysokosc obrazka
	lw	s11, 24(s8)
	li	s6, 0x3			# padding: szerokosc % 4
	and	s6, s10, s6
	add	t1, s10, s10
	add	t1, t1, s10
	add	s9, s6, t1		# przeskok = 3*szerokosc + padding
	addi	s10, s10, -1
	addi	s11, s11, -1
	mv	s0, s8
	li	s5, 0
	li	s4, 0
	li	s3, 0
# Rejestry zachowywane:
# s10, s11 - maksymalne x i y
# s9 -  ilosc bajtow, by przeskoczyc na (x, y+1)
# s7, s8 - wskaznik na obecnie przetwarzany bajt w mapie i w buforze na nowe piksele
# s6 - ilosc bajtow w paddingu
# s5 - ilosc przechowywanych aktualnie bajtow w buforze
# s3, s4 - x i y obecnie przetwarzanego piksela
# s2 - ilosc pikseli w obecnie przetwarzanym oknie (pojawia sie pozniej)
# s1 - powyzej tej ilosci bajtow w buforze nalezy przepisac zawartosc do pliku (pojemnosc bufora - 3 - maksymalny padding (3))
# s0 - poczatek bufora na nowe piksele
make_window:
	addi	t3, s3, -2		# Przygotowanie okna z pikselami do przetworzenia
	bgez 	t3, xmax		# t3, t4, t5, t6 - xmin, xmax, ymin, ymax
	li	t3, 0			# t2 - wskaznik na dane, t1 - offset, by przeskoczyc z konca wyzszego rzedu na poczatek nizszego
xmax:	
	addi	t4, s3, 2
	ble	t4, s10, ymin
	mv	t4, s10
ymin:
	addi	t5, s4, -2
	bgez	t5, ymax
	li	t5, 0
ymax:
	addi	t6, s4, 2
	ble	t6, s11, move_reader
	mv	t6, s11
move_reader:				# Przesuwanie czytnika na pierwszy piksel w oknie
	sub	t0, s3, t3
	sub	t2, s7, t0
	slli	t0, t0, 1
	sub	t2, t2, t0		# czytnik = wskaznik_na_obecny_piksel - 3*(x-xmin)
	sub	t0, t6, s4
	beqz	t0, skip
	addi	t0, t0, -1
	sll	t0, s9, t0
skip:	add	t2, t2, t0		# czytnik += skok_o_linie*(ymax-y)
	sub	t0, t4, t3		# Wyznaczanie offsetu
	addi	t0, t0, 1	
	add	t1, t0, t0
	add	t1, t1, t0		
	add	t1, s9, t1		
	neg	t1, t1			# przeskok = -szerokosc_rzedu - 3*(xmax-xmin+1)
	la	a5, filter_buffer_red	# Przygotowanie wczytywania danych z okna
	la	a6, filter_buffer_green	# a3, a4: x i y odczytywanego piksela
	la	a7, filter_buffer_blue	# a5, a6, a7: wskazniki do buforow z danymi
	mv	a3, t3
	mv	a4, t6 
gather_data:
	lb	t0, (t2)		# przepisywanie po 1 kolorze do buforow
	sb	t0, (a7)		# kolory sa w kolejnosci: b, g, r
	addi	t2, t2, 1
	lb	t0, (t2)
	sb	t0, (a6)
	addi	t2, t2, 1
	lb	t0, (t2)
	sb	t0, (a5)
	addi	t2, t2, 1		# przesuwanie buforow i x
	addi	a3, a3, 1
	addi	a5, a5, 1
	addi	a6, a6, 1
	addi	a7, a7, 1
	ble	a3, t4, gather_data
	addi	a4, a4, -1		# koniec rzedu
	add	t2, t2, t1
	mv	a3, t3
	bge	a4, t5, gather_data
calculate:				# koniec wczytywania danych z okna
	sub	a0, t4, t3		# liczenie ilosci pikseli
	sub	a1, t6, t5
	addi	a0, a0, 1
	addi	a1, a1, 1
	mul 	s2, a0, a1
	la	a0, filter_buffer_red	# obliczenie sredniej dla nowych pikseli i ich zapis
	jal	calculate_median
	sb	a0, 2(s8)
	la	a0, filter_buffer_green
	jal	calculate_median
	sb	a0, 1(s8)
	la	a0, filter_buffer_blue
	jal	calculate_median
	sb	a0, (s8)		# przesuniecie buforow i x
	addi	s3, s3, 1
	addi	s5, s5, 3
	addi	s7, s7, 3
	addi	s8, s8, 3
	ble	s5, s1, no_write
	lw	a0, file_descriptor	# przepisanie bufora
	mv	a1, s0
	mv	a2, s5
	li	a7, WRITE	
	ecall
	mv	s8, s0
	li	s5, 0
no_write:
	ble	s3, s10, make_window
	li	s3, 0			# koniec rzedu, trzeba dodac padding i zmienic wspolrzedne
	addi	s4, s4, 1		
	add	s7, s7, s6		
	add	s8, s8, s6
	add	s5, s5, s6
	ble 	s4, s11, make_window
write_and_finish:			# koniec pliku
	lw	a0, file_descriptor	# przepisanie pozostalych pikseli
	mv	a1, s0
	mv	a2, s5
	li	a7, WRITE			
	ecall
	lw	a0, file_descriptor	# zamkniecie pliku
	li	a7, CLOSE
	ecall
	li	a7, EXIT		# koniec programu
	ecall
calculate_median:
	mv	a1, s2			# a0: adres poczatku, a1: pozostale piksele, a2: uzywany adres
	li	a7, 15			# a7: stale 15
	ble 	a1, a7, calculate_average # <15 pikseli - od razu do liczenia sredniej
median_loop_start:			# wyrzucenie 2 liczb z konca, najwieksza i najmniejsza napotkana pozostaje w rejestrach
	add	a2, a0, a1		# a2 teraz wskazuje na pierwszy nieuzywany adres
	lb	a3, -1(a2)		# a3: najmniejsza, a4: najwieksza
	andi	a3, a3, 0xff		# reszta rejestru nie musi byc zerem
	lb	a4, -2(a2)
	andi	a4, a4, 0xff
	addi	a2, a2, -3
	addi	a1, a1, -2
	srli	a2, a2, 2
	slli	a2, a2, 2		# floor adresu do 4
	andi	t3, a1, 0x3		# t3: ilosc liczb w slowie
	bnez	t3, skip2
	addi	t3, t3, 4
skip2:	addi	t4, t3, -1
	slli	t4, t4, 3		# t4: offset liczby w slowie: 8*(ilosc liczb - 1)
	ble	a3, a4, median_loop_lw
	mv	a5, a3
	mv	a3, a4
	mv	a4, a5
median_loop_lw:
	li	t6, 0			# t6: slowo do zapisu, t5: slowo do odczytu
	lw	t5, (a2)
median_loop:				# przechodzenie po kazdej liczbie i sprawdzanie, czy jest wieksza/mniejsza od skrajnych
	srl	a5, t5, t4	
	andi	a5, a5, 0xff		# a5: nowa
	bgt	a5, a4, swap_big
	bge	a5, a3, check
swap_small:
	mv	a6, a3
	mv	a3, a5
	mv	a5, a6
	b	check
swap_big:
	mv	a6, a4
	mv	a4, a5
	mv	a5, a6
check:
	srl	t6, t6, t4		# przepisanie liczby
	add	t6, t6, a5
	sll	t6, t6, t4
	addi	t3, t3, -1
	addi	t4, t4, -8
	bnez	t3, median_loop		# czy zostaly bajty w slowie
	sw	t6, (a2)
	addi	a2, a2, -4
	li	t3, 4
	li	t4, 24
	bge	a2, a0, median_loop_lw	# czy jest jeszcze co czytac
	bgt	a1, a7, median_loop_start # czy nalezy usunac wiecej pikseli
	beq	a1, a7, calculate_average
	sb	a3, 14(a0)		# jest 14 pikseli, trzeba dodac ostatni, najmniejszy
	li	a1, 15
calculate_average:			# a0: adres poczatku, a1: ilosc pikseli (a3: w ostatnim slowie), a2: uzywany adres, a6: suma
	add	a2, a0, a1		# a2 teraz wskazuje na pierwszy nieuzywany adres
	andi	a3, a1, 0x3
	addi	a2, a2, -1
	srli	a2, a2, 2
	slli	a2, a2, 2		# floor adresu do 4
	lw	a5, (a2)
	li	a6, 0
	beqz	a3, avg_4		# dodawanie n bajtow za pierwszym razem
	addi	a3, a3, -1
	beqz	a3, avg_1
	addi	a3, a3, -1	
	beqz	a3, avg_2
	b	avg_3
avg_loop:
	addi	a2, a2, -4
	lw	a5, (a2)		# a5: cale slowo, a4: bajt
avg_4:	andi	a4, a5, 0xff		# przesuwanie na odpowiedni bajt i dodawanie
	add	a6, a6, a4
	srli	a5, a5, 8
avg_3:	andi	a4, a5, 0xff
	add	a6, a6, a4
	srli	a5, a5, 8
avg_2:	andi	a4, a5, 0xff
	add	a6, a6, a4
	srli	a5, a5, 8
avg_1:	andi	a4, a5, 0xff
	add	a6, a6, a4
	bne	a2, a0, avg_loop
	divu	a0, a6, a1
	jr	ra
